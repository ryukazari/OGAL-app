import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import { ExpedientesService } from '../../services/expedientes.service';
@Component({
  selector: 'app-listar-expediente',
  templateUrl: './listar-expediente.component.html',
  styleUrls: ['./listar-expediente.component.css']
})
export class ListarExpedienteComponent implements OnInit {
  procedimientos: string[] = [
    'Opcion1', 'Opcion2', 'Opcion3'
  ];
  estados: string[] = [
    'Estado1', 'Estado2', 'Estado3'
  ];
  demandante: string = 'LOPEZ NOLORBE, JUAN LUIS';
  demandado: string = 'UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS';

  

  displayedColumns: string[] = ['expediente'];

  //dataSource: MatTableDataSource<iExpediente>;
  dataSource = new MatTableDataSource<iExpediente>(expedientes);
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(
    private _expedienteServicio: ExpedientesService
  ) { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;

    //this.listarExpedientes();
  }
/*
  listarExpedientes(){
    this._expedienteServicio.listarExpediente().subscribe(
      res=>{
        console.log(res);
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
      },
      error=>{
        console.log(error)
      }

    )
  }*/

}

function mostrarDetalle(expediente: Object){
  console.log(expediente);
}

export interface iExpediente {
  codigo_expediente:String;
  cod_distrito_judicial:String;
  demandante:String;
  demandado:String;
  materia:String;
  estado:String;
}




const expedientes: iExpediente[] = [
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-01",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"ESTADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-02",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"INICIADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-03",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"FINALIZADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-04",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"INICIADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-05",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"CHUPETIN TRUJILLO",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"INICIADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-06",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"ORESTES CACHAY",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"PROCESADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-07",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"INICIADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-08",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"INICIADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-09",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"FINALIZADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-10",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"FINALIZADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-11",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"INICIADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-12",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"PROCESADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-13",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"PROCESADO"
  },
  {
      "codigo_expediente":"00819-2009-0-1903-JR-CI-14",
      "cod_distrito_judicial":"JUZGADO DE TRABAJO TRANSITORIO DE MAYNAS",
      "demandante":"LOPEZ NOLORBE, JUAN LUIS",
      "demandado":"UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS",
      "materia":"ACCION CONTENCIOSA ADMINISTRATIVA",
      "estado":"INICIADO"
  }
];