import { Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-bandeja-informacion',
  templateUrl: './bandeja-informacion.component.html',
  styleUrls: ['./bandeja-informacion.component.css']
})
export class BandejaInformacionComponent implements OnInit {

  displayedColumns: string[] = ['correos'];

  dataSource = new MatTableDataSource<iCorreo>(correos);
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor() { }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}

export interface iCorreo {
  asunto:String;
  fecha:String;
  remitente:String;
  materia:String;
}

const correos : iCorreo[] = [
  {
    "asunto":"ASUNTO 1",
    "fecha":"12/03/2019",
    "remitente":"LOPEZ NOLORBE, JUAN LUIS",
    "materia":"ACCION CONTENSIOSA ADMINISTRATIVA"
  },
  {
    "asunto":"ASUNTO 2",
    "fecha":"12/03/2019",
    "remitente":"LOPEZ NOLORBE, JUAN LUIS",
    "materia":"ACCION CONTENSIOSA ADMINISTRATIVA"
  },
  {
    "asunto":"ASUNTO 3",
    "fecha":"12/03/2019",
    "remitente":"LOPEZ NOLORBE, JUAN LUIS",
    "materia":"ACCION CONTENSIOSA ADMINISTRATIVA"
  },
  {
    "asunto":"ASUNTO 4",
    "fecha":"12/03/2019",
    "remitente":"LOPEZ NOLORBE, JUAN LUIS",
    "materia":"ACCION CONTENSIOSA ADMINISTRATIVA"
  },
  {
    "asunto":"ASUNTO 5",
    "fecha":"12/03/2019",
    "remitente":"LOPEZ NOLORBE, JUAN LUIS",
    "materia":"ACCION CONTENSIOSA ADMINISTRATIVA"
  }
]