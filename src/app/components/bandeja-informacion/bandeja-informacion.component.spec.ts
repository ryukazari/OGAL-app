import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BandejaInformacionComponent } from './bandeja-informacion.component';

describe('BandejaInformacionComponent', () => {
  let component: BandejaInformacionComponent;
  let fixture: ComponentFixture<BandejaInformacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BandejaInformacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BandejaInformacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
