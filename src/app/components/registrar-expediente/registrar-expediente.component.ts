import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registrar-expediente',
  templateUrl: './registrar-expediente.component.html',
  styleUrls: ['./registrar-expediente.component.css']
})
export class RegistrarExpedienteComponent implements OnInit {
  
  items: Object[] = [
    {value: 'DEMANDANTE', viewValue: 'DEMANDANTE'},
    {value: 'DEMANDADO', viewValue: 'DEMANDADO'}
  ];
  personas: Object[] = [
    {value: 'NATURAL', viewValue: 'NATURAL'},
    {value: 'JURIDICA', viewValue: 'JURÍDICA'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
