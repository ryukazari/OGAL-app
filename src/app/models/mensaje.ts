export class Mensaje{

    constructor(
        codigo_dependencia: Number,
        codigo_expediente: String,
        asunto: String,
        origen: String,
        destinario: String,
        mensaje: String
    ){}
}