export class Seguimiento{

    constructor(
        codigo: Number,
        fecha_de_ingreso: Date,
        resolucion: String,
        actos: String,
        folios: Number,
        tipo_de_modificacion: String,
        proveido: Date,
        sumilla: String
    ){}
}