export class Expediente{

    constructor(
        codigo_expediente: String,
        tipo: String,
        cod_distrito_judicial: String,
        especialista_legal: String,
        juez: String,
        especialidad: String,
        proceso: String,
        fecha_de_inicio: Date,
        fecha_de_conclusion: Date,
        materia: String,
        motivo_de_conclusion: String,
        ubicacion: String,
        sumilla: String
    ){}
}