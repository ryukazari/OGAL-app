import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Global } from './global';

@Injectable({
  providedIn: 'root'
})
//@Injectable()
export class ExpedientesService {
  public url: string;

  constructor(private _http: HttpClient) {
    this.url = Global.url
  }

  listarExpediente(): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.get(this.url + 'expedientes', { headers: headers });
  }

  obtenerExpediente(id): Observable<any>{
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.get(this.url+'detalle-expedientes/'+id ,{ headers:headers });
  }
}
