import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Global } from './global';

@Injectable({
  providedIn: 'root'
})
//@Injectable()
export class SeguimientoService {
  public url: string;

  constructor(private _http: HttpClient) {
    this.url = Global.url
  }

  listarSeguimiento(): Observable<any> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this._http.get(this.url + 'seguimientos', { headers: headers });
  }
}
